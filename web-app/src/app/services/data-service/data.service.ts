import { Injectable } from '@angular/core';
import {Http, RequestOptions, Response, RequestOptionsArgs} from '@angular/http';
import {API} from '../../types/api';
import { Observable } from 'rxjs/rx';

@Injectable()
export class DataService {

  private API: API = new API('localhost', 4200, 'api', false);

  constructor(private http: Http) {
    console.log('HTTP Service initialized');
  }


  getStats(): Observable<any> {
    const api = this.API.get('stats');
    return this.http.get(api.url, api.options)
      .map(this.extractData).catch(this.handleError);
  }

  ask(query: string): Observable<any> {
    const body: {
      from?: any;
      size?: any;
      query: any
    } = {
      query : query
    };

    const api = this.API.put('ask', body);

    return this.http.put(api.url, api.body, api.options)
        .map(this.extractData).catch(this.handleError);
  }
  searchByAny(wildcard: string, from?: number, size?: number): Observable<any> {
    const body: {
      from?: any;
      size?: any;
      query: any
    } = {
      query : wildcard
    };

    if (from && size) {
      body.from = from;
      body.size = size;
    }

    const api = this.API.put('search', body);

    return this.http.put(api.url, api.body, api.options)
      .map(this.extractData).catch(this.handleError);
  }

  resetFiles(): PromiseLike<any> {
    const api = this.API.delete('files');
    return this.http.delete(api.url, api.options).toPromise();
  }


  processFiles(): PromiseLike<any> {
    const api = this.API.put('files');
    return this.http.put(api.url, api.options).toPromise();
  }

  indexFiles(): PromiseLike<any> {
    const api = this.API.put('index');
    return this.http.put(api.url, api.options).toPromise();
  }

  createIndex(): PromiseLike<any> {
    const api = this.API.post('index');
    return this.http.post(api.url, api.options).toPromise();
  }

  private extractData(res: Response) {
    const body = res.json();
    return body || { };
  }

  private handleError (error: Response | any) {
    // In a real world app, you might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

}
