import {AfterViewInit, Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {DataService} from '../services/data-service/data.service';
import {ModalDismissReasons, NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {QueryStateI} from '../types/query-state';


@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SettingsComponent implements OnInit {

  state: QueryStateI;

  stats: {
    unprocessed: number,
    processed: number
  };

  closeResult: string;

  @ViewChild('content')
  content: ViewChild;

  private modalRef: NgbModalRef;

  constructor(private dataService: DataService,
              private modalService: NgbModal) { }

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    this.open(this.content);

    this.dataService.getStats().toPromise()
      .then(res => {
        console.log(res);
        this.stats = res;
        this.modalRef.close();
      })
      .catch(err => {
        console.error(err);
        this.modalRef.close();
      });
  }

  onClick(name: string) {
    this.open(this.content);

    switch (name) {
      case 'create':
        this.createIndex();
        break;
      case 'index':
        this.indexFiles();
        break;
      case 'process':
        this.processFiles();
        break;
      case 'reset':
        this.resetFiles();
        break;
    }
  }

  handleResult = () => {
    this.modalRef.close();
    this.loadData();
  }

  processFiles() {
    this.dataService.processFiles().then(
      this.handleResult,
      rej => {
        console.error(rej);
        this.modalRef.close();
      }
    );
  }

  indexFiles() {
    this.dataService.indexFiles().then(
      this.handleResult,
      rej => {
        console.error(rej);
        this.modalRef.close();
      }
    );
  }

  resetFiles() {
    this.dataService.resetFiles().then(this.handleResult,
      rej => {
        console.error(rej);
        this.modalRef.close();
      }
    );
  }

  createIndex() {
    this.dataService.createIndex().then(this.handleResult,
      rej => {
        console.error(rej);
        this.modalRef.close();
      }
    );
  }

  open(content) {
    this.modalRef = this.modalService.open(content, { windowClass: 'loading-modal', backdrop: 'static' });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

}
