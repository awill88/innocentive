import { Component } from '@angular/core';
import {DataService} from './services/data-service/data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  constructor(dataService: DataService) {

  }
}
