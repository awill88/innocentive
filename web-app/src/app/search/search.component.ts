import { Component, OnInit } from '@angular/core';
import {isNullOrUndefined} from 'util';
import {Interrogatives} from '../types/interrogative';
import {QueryState, QueryStateI} from '../types/query-state';
import {DataService} from '../services/data-service/data.service';
import { SearchResponse } from 'elasticsearch';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  title = 'Enter your question';
  question: string  = 'What developments related to artificial intelligence are most impactful to the national security of the United States?';
  btnText = 'GO';
  state: QueryStateI = QueryState.EMPTY;
  expanded: any;

  results: SearchResponse<{}>;

  pageSize = 10;
  page = 1;





  cancel() {
    this.question = null;
    this.state = QueryState.EMPTY;
  }

  onAsk(evt: Event) {
    if (this.question && this.question.length > 0) {

      // Do not accept not enter keys
      if (evt instanceof KeyboardEvent && (<KeyboardEvent>evt).code !== 'Enter') {
        return;
      }

      // TODO Enforce only questions in queries
      // if (this.question.endsWith('?')) {
      //   this.question = this.question.substring(0, this.question.length - 1);
      // }
      //
      // if (!this.isValidQuestion()) return;

      this.state = QueryState.LOADING;

      this.page = 1;
      this.dataService.searchByAny(this.question, this.page - 1, this.pageSize).toPromise().then(
        (results: SearchResponse<any>) => {
          this.state = QueryState.RESULT;
          this.results = results;
        }
      ).catch(err => {
        console.error(err);
        this.state = QueryState.ERROR;

      });



    }
  }

  onPageChange(pageNum) {
    this.page = pageNum;
    this.dataService.searchByAny(this.question, this.page - 1, this.pageSize).toPromise().then(
      (results: SearchResponse<any>) => {
        this.state = QueryState.RESULT;
        this.results = results;
      }
    ).catch(err => {
      console.error(err);
      this.state = QueryState.ERROR;

    });
  }

  getQuestionArray(): Array<string> {
    return this.question.split(' ');
  }


  constructor(private dataService: DataService) {

  }

  ngOnInit() {
  }

  isValidQuestion() {
    const firstWord = this.getQuestionArray()[0].toUpperCase();
    return !isNullOrUndefined(Interrogatives[firstWord]);
  }

  toggleOrSelect(source: any) {
    if (source === this.expanded) {
      this.expanded = null;
      return;
    }

    this.expanded = source;
  }

  getNumPages(hits) {
    return Math.ceil(hits.total / this.pageSize);
  }

}
