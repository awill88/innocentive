export class Endpoint {
  path: Array<string>
  params: Array<string>

  constructor(path: Array<string>, params: Array<string>) {
    this.path = path;
    this.params = params;
  }

  prepare(args?: Array<string>) {
    return this.path.join('/')
  }
}
