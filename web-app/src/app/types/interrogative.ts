export type Interrogative = 'who' | 'what' | 'where' | 'when' | 'how';

export const Interrogatives = {
  WHO: 'who' as Interrogative,
  WHAT: 'what' as Interrogative,
  WHERE: 'where' as Interrogative,
  WHEN: 'when' as Interrogative,
  HOW: 'how' as Interrogative
};
