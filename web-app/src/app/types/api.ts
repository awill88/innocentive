import {Endpoint} from './endpoint';
import {RequestOptions, Headers, RequestOptionsArgs} from '@angular/http';

// This class will construct API calls to the server. It will be initialized by an angular service
export class API {
  scheme: string; // HTTP or HTTPS
  host: string;   // localhot or otherwise
  port: number;   // port number
  base: string;   // base path to start from

  // Below are the implemented API endpoints
  settings: Endpoint = new Endpoint(['settings'], null);
  stats: Endpoint = new Endpoint(['data', 'stats'], null);
  search: Endpoint = new Endpoint(['search'], null);
  ask: Endpoint = new Endpoint(['ask'], null);
  files: Endpoint = new Endpoint(['data', 'convert'], null);
  index: Endpoint = new Endpoint(['data', 'index'], null);

  constructor(host: string, port: number, base: string, https: boolean) {
    this.host = host;
    this.port = port;
    this.base = base;
    this.scheme = https ? 'https' : 'http';
  }

  build(endpoint: Endpoint) {
    return `${this.scheme}://${this.host}${[80, 443].includes(this.port) ? '' : ':' + this.port}/${this.base}/${endpoint.prepare()}`;
  }

  get(endpoint: string): {url: string, options: RequestOptionsArgs} {
    if (!Object(this).hasOwnProperty(endpoint)) throw new Error('Not a valid API endpoint');
    const location = this.build(Object(this)[endpoint]);

    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });

    return {url: location, options: options};
  }

  post(endpoint: string, body?: any) {
    if (!Object(this).hasOwnProperty(endpoint)) throw new Error('Not a valid API endpoint');
    const location = this.build(Object(this)[endpoint]);

    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });

    return {url: location, body: body, options: options};
  }

  put(endpoint: string, body?: any) {
    if (!Object(this).hasOwnProperty(endpoint)) throw new Error('Not a valid API endpoint');
    const location = this.build(Object(this)[endpoint]);

    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });

    return {url: location, body: body, options: options};
  }

  delete(endpoint: string, body?: any) {
    if (!Object(this).hasOwnProperty(endpoint)) throw new Error('Not a valid API endpoint');

    const location = this.build(Object(this)[endpoint]);

    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });

    return {url: location, options: options};
  }

}
