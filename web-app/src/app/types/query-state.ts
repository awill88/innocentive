export type QueryStateI = 'EMPTY' | 'CHANGED' | 'LOADING' | 'RESULT' | 'ERROR';

export const QueryState = {
  EMPTY: 'EMPTY' as QueryStateI,
  CHANGED: 'CHANGED' as QueryStateI,
  LOADING: 'LOADING' as QueryStateI,
  RESULT: 'RESULT' as QueryStateI,
  ERROR: 'ERROR' as QueryStateI
};
