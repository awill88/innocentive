import { InnocentiveWebAppPage } from './app.po';

describe('innocentive-web-app App', () => {
  let page: InnocentiveWebAppPage;

  beforeEach(() => {
    page = new InnocentiveWebAppPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
