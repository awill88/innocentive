# Setup


## Prerequisite Software (macOS)
1. Homebrew

    On your terminal:
    
    `ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"`

2. Docker for MacOS

    ` https://docs.docker.com/docker-for-mac/install/#download-docker-for-mac ` 


## Environment Setup

After cloning this repository, open your terminal and enter:

`cd DIRECTORY_YOU_CLONED_TO/innocentive`

Now enter the following sequence of commands

`brew install python`

`pip install --upgrade virtualenv`

`virtualenv api`

`source api/bin/activate`

`pip install -r api/requirements.txt`

### Running ElasticSearch

`docker-compose up`

### Running the server

`source api/bin/activate`

`python api/run.py`

### Running Angular CLI

If node is not installed, run:

`brew install node`

`npm -v`

`npm install -g @angular/cli@latest`

`cd web-app`

`npm install`

`ng serve`