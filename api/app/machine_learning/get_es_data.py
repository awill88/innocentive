from elasticsearch import Elasticsearch,helpers
import io
import matplotlib.pyplot as plt
import tensorflow as tf


es = Elasticsearch()

res = es.search(index='articles',doc_type='articles',body={
    "from": 0,
    "size": 10000,
	"_source": ["title","type","date"],
  "query":{

      "query_string": {
            "query": "artificial intelligence",
            "fields": [
                "title",
                "text",
                "tags"
            ]
        }
  }
})
import pandas as pd
data_points = []
for hit in res['hits']['hits']:
    if(pd.to_datetime(hit['_source']['date'], format='%m/%d/%Y') is not None):
        hit['date'] = pd.to_datetime(hit['_source']['date'], format='%m/%d/%Y')
        print((hit['date']) is None)
        data_points.append(hit)


dates = [d['date'] for d in data_points]

plt.scatter(dates, [s['_score'] for s in data_points], s =100, c = 'red')
plt.show()