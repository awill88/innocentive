
def query_by_type(type):
    return {
        "query": {
            "term": {"type": type}
        }
    }

def query_by_author(author):
    return {
        'query': {
            'term': { 'author': author}
        }
    }

def query_by_wildcard(term,regex='*'):
    return {
        "query": {
            "wildcard": {term: regex}
        }
    }

def count_from_response(response):
    if dict(response).has_key('count'):
        return response['count']
    return 0;