import re
from elasticsearch.helpers import BulkIndexError
from app import app
import json
from flask import Response, request
from flask_api import status
import urllib2
import urllib
import os
from collections import OrderedDict
from xmljson import BadgerFish
from xml.etree.ElementTree import Element, tostring, fromstring
from xmljson import parker, Parker
import os.path
from elasticsearch import Elasticsearch,helpers
from datetime import datetime
import queries
import os
import multiprocessing

# __file__ refers to the file settings.py
APP_ROOT = os.path.dirname(os.path.abspath(__file__))   # refers to application_top
APP_STATIC = os.path.join(APP_ROOT, 'static')

elasticsearch_host = 'http://localhost'
elasticsearch_port = 9200
unprocessed_path = '../data/unprocessed'
processed_path = '../data/processed'
debug = False

default_index = 'articles'
default_types = 'articles'
default_index_file = APP_STATIC + '/default_index.json'

es = Elasticsearch()
bf = BadgerFish(dict_type=OrderedDict)




def worker():
    unprocessed_files = [f for f in os.listdir(unprocessed_path) if re.match(r'[0-9]+.*\.xml', f)]
    file_content = []

    for f in unprocessed_files:
        # Only process new stuff
        if os.path.exists(processed_path + '/' + f.replace('xml', 'json')) is False:
            with open(unprocessed_path + '/' + f, 'r') as file:
                contents = ''
                for line in file.readlines():
                    contents = contents + line.replace('\n', '')
                # Append to list of processed files
                xml_data = parker.data(fromstring(contents.replace('  ', '').replace('\xc2\xa0', ' ')))
                xml_data['filename'] = f

                # Make nested objects look right for json
                if xml_data['tags'] is not None and dict(xml_data['tags']).has_key('tag'):
                    # If tags is a string value, force it into an array
                    if type(xml_data['tags']['tag']) == str:
                        xml_data['tags'] = [xml_data['tags']['tag']]
                    else:
                        xml_data['tags'] = xml_data['tags']['tag']
                else:
                    xml_data['tags'] = []

                xml_data['departments'] = xml_data['departments']['department'] if xml_data[
                                                                                       'departments'] is not None and dict(
                    xml_data['departments']).has_key('department') else []
                xml_data['imgalttext'] = xml_data['imgalttext']['img'] if xml_data['imgalttext'] is not None and dict(
                    xml_data['imgalttext']).has_key('img') else []

                for key in xml_data.keys():
                    # trim by default
                    if key == 'text' and xml_data[key] is not None:
                        xml_data[key] = xml_data[key].lstrip()

                    # parse dates
                    if xml_data.get(key) is not None and key == 'date':
                        xml_data[key] = parse_date(xml_data.get(key))

                file_content.append(xml_data)
                file.close()
                # Debug mode
                if len(file_content) > 10 and debug:
                    break;

        for c in file_content:
            filepath = str(processed_path + '/' + c['filename']).replace('xml', 'json')
            with open(filepath, 'w') as file:
                file.write(json.dumps(c));
                file.close()


processJob = multiprocessing.Process(target=worker)

# Helper function
def json_res_from_dict(data):
    response = Response(json.dumps(data))
    response.headers['Content-Type'] = 'application/json'
    return response

@app.route('/api/settings')
def info():
    query = urllib2.urlopen(elasticsearch_host+':'+str(elasticsearch_port))
    data = json.load(query)
    return json_res_from_dict(data)

# Return statistics on the dataset
@app.route('/api/data/stats')
def stats():

    try:
        print(es.count(index=default_index,
                       doc_type=default_types,
                       body=queries.query_by_type('article')
                       ))
        data = {
        'unprocessed': len([f for f in os.listdir(unprocessed_path) if re.match(r'[0-9]+.*\.xml', f)]),
        'processed': len([f for f in os.listdir(processed_path) if re.match(r'[0-9]+.*\.json', f)]),
        'types' : {
            'article': queries.count_from_response(
                es.count(index=default_index,
                                 doc_type=default_types,
                                 body=queries.query_by_type('article')
                                 )
            ),
            'blog': queries.count_from_response(
                es.count(index=default_index,
                                 doc_type=default_types,
                                 body=queries.query_by_type('blog')
                                 )
            )
        },
        'authors': {
            'total': queries.count_from_response(
                es.count(index=default_index,
                                 doc_type=default_types,
                                 body=queries.query_by_wildcard('author'))
            )
        }

        }
    except:
        data = {
            'unprocessed': len([f for f in os.listdir(unprocessed_path) if re.match(r'[0-9]+.*\.xml', f)]),
            'processed': len([f for f in os.listdir(processed_path) if re.match(r'[0-9]+.*\.json', f)]),
            'types': {
                'article': 0
                ,
                'blog': 0
            },
            'authors': {
                'total': 0
            }
        }

    # get number of articles
    return json_res_from_dict(data)

# This will create or replace the index
@app.route('/api/data/index',methods=['POST','DELETE','PUT'])
def index_data():

    # Creates document entries on the created index
    if request.method == 'PUT':
        files = [f for f in os.listdir(processed_path) if re.match(r'[0-9]+.*\.json', f)]
        actions = []

        #delete all documents (overwrite)
        for f in files:
            temp = {
                '_op_type': 'delete',
                '_index': default_index,
                '_id': int(f.replace('.json', '')),
                '_type': 'articles'
            }

            actions.append(temp)

        try:
            response = helpers.bulk(es, actions=actions)
        except BulkIndexError as be:
            response = be[1]

        actions = []
        try:
            for f in files:
                with open(processed_path + '/' + f, 'r') as file:
                    contents = ''
                    for line in file.readlines():
                        contents = contents + line.replace('\n', '')
                    data = json.loads(contents)
                    temp = {
                        '_op_type': 'create',
                        '_index': default_index,
                        '_id': int(f.replace('.json','')),
                        '_type': default_types,
                        '_source': contents
                    }
                    actions.append(temp)
                    file.close()

            response = helpers.bulk(es,actions=actions,stats_only=True,ignore=409)
        except BulkIndexError as be:
            response = be[1]
        except Exception as e:
            response = e

    # Creating an inex
    elif request.method == 'POST':
        # By default remove old index
        es.indices.delete(index=default_index, ignore=[400, 404])
        body = None
        with open(default_index_file,'r') as file:
            body = json.loads(str.join('',file.readlines()).replace('\n','').replace(' ',''))
            file.close()

        response = es.indices.create(index=default_index, body=body, ignore=400)

    elif request.method == 'DELETE':
        result = es.indices.delete(index=default_index, ignore=[400, 404])

        if dict(result).has_key('status'):
            return json_res_from_dict(result), result['status']
        else:
            return json_res_from_dict(result)

    # Return the new index
    return json_res_from_dict(response)

@app.route('/api/data/convert', methods=['PUT','DELETE'])
def convert():
    unprocessed_files = [f for f in os.listdir(unprocessed_path) if re.match(r'[0-9]+.*\.xml', f)]
    data = {}

    # Delete the processed files
    if request.method == 'DELETE':

        # Delete from elasticsearch
        es.delete_by_query(index=default_index,doc_type=default_types,body={
            'query': {
                'match_all': {}
            }
        })

        for f in unprocessed_files:
            # Only process new stuff
            if os.path.exists(processed_path+'/'+f.replace('xml','json')) is True:
                os.remove(processed_path+'/'+f.replace('xml','json'))

        data['num_deleted'] = len(unprocessed_files)

    # Process the files
    else:
        if(processJob.is_alive() is False):
            processJob.start()
        data = { 'working': processJob.is_alive() }

    return json_res_from_dict(data)


@app.route('/api/search', methods=['PUT'])
def search():
    try:
        data = es.search(index=default_index,doc_type=default_types,body=request.data)
    except Exception as e:
        data = e

    return json_res_from_dict(data)



def parse_date(date_str):
    try:
        datetime_object = datetime.strptime(date_str, '%B %d, %Y')
    except ValueError as ve:
        try:
            datetime_object = datetime.strptime(date_str, '%B %Y')
        except ValueError as ve1:
            datetime_object = None
    return datetime_object.strftime('%m/%d/%Y') if datetime_object is not None else None

